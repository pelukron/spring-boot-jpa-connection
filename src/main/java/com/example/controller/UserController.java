package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.User;
import com.example.repository.UserRepository;

@RestController
public class UserController {
	
	private UserRepository userRepository;

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@PostMapping("/create")
	public String createUser(String email, String name){
		User user = null;
		try{
			user = new User(email, name);
			userRepository.save(user);
		} catch (Exception e) {
			// TODO: handle exception
			return "Error "+e.getMessage();
		}
		return "User succesfully created! (id = " + user.getId() + ")";
	}
	
	@RequestMapping("/all")
	public Iterable<User> findAll(){
		return userRepository.findAll();
	}
	
	@PostMapping("/get_email")
	public User findByEmail(String email){
		return userRepository.findByEmail(email);
	}
	

}
